<!DOCTYPE html>
<html>
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="@nytimes">
<meta name="twitter:creator" content="@SarahMaslinNir">
<meta name="twitter:title" content="Parade of Fans for Houston’s Funeral">
<meta name="twitter:description" content="NEWARK - The guest list and parade of limousines with celebrities emerging from them seemed more suited to a red carpet event in Hollywood or New York than than a gritty stretch of Sussex Avenue near the former site of the James M. Baxter Terrace public housing project here.">
<meta name="twitter:image" content="http://graphics8.nytimes.com/images/2012/02/19/us/19whitney-span/19whitney-span-articleLarge.jpg">
<head lang="en">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <meta charset="UTF-8">
    <title></title>
</head>

<body>
<h1 id="testText"> teste</h1>

<div id="fb-root"></div>
<div class="fb-login-button" data-max-rows="1" data-size="medium" data-show-faces="true" data-auto-logout-link="true"></div>

<br>
<div class="fb-like"  data-layout="button"
     data-action="like"
     data-show-faces="false"
     data-share="true"></div>
<br>
<br>
<button id="shareIT">Try it</button>

<br>
<br>

<a href = 'twitter/redirect.php'>Sign In with twitter</a>

</body>
<script src="js/config.js"></script>

<script src="js/app.js"></script>
</html>