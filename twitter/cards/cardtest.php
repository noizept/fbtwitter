<?php
/**
 * Created by IntelliJ IDEA.
 * User: sergio
 * Date: 6/21/15
 * Time: 3:13 AM
 * Documentation on : https://github.com/niallkennedy/twitter-cards-php for the twitter-card.php
 */
// load Twitter_Card
require "twitter-card.php";

$card = new Twitter_Card();
$card->setURL( 'http://www.nytimes.com/2012/02/19/arts/music/amid-police-presence-fans-congregate-for-whitney-houstons-funeral-in-newark.html' );
$card->setTitle( 'Parade of Fans for Houston\'s Funeral' );
$card->setDescription( 'NEWARK - The guest list and parade of limousines with celebrities emerging from them seemed more suited to a red carpet event in Hollywood or New York than than a gritty stretch of Sussex Avenue near the former site of the James M. Baxter Terrace public housing project here.' );
// optional
$card->setImage( 'http://graphics8.nytimes.com/images/2012/02/19/us/19whitney-span/19whitney-span-articleLarge.jpg', 600, 330 );
$card->setSiteAccount( 'nytimes');
$card->setCreatorAccount( 'SarahMaslinNir');

// echo a string of <meta> elements
echo $card->asHTML();
?>