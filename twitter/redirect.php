<?php
/**
 * Created by IntelliJ IDEA.
 * User: sergio
 * Date: 6/20/15
 * Time: 4:08 AM
 */
error_reporting(-1);
ini_set("display_errors", "On");
session_start();

require "twitteroauth-master/autoload.php";
require "config.php";
use Abraham\TwitterOAuth\TwitterOAuth;

$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET);
$request_token = $connection->oauth('oauth/request_token', array('oauth_callback' => OAUTH_CALLBACK));
$_SESSION['oauth_token'] = $token =$request_token['oauth_token'];
$_SESSION['oauth_token_secret'] = $request_token['oauth_token_secret'];
$url = $connection->url('oauth/authorize', array('oauth_token' => $request_token['oauth_token']));


// Header not working, asked for help maybe some other plugin is outputing data?
//  for now using JS to do it
printf("<script>location.href='" .$url."'</script>");

//header('Location : '. $url);

?>