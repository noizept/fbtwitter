<?php
/**
 * Created by IntelliJ IDEA.
 * User: sergio
 * Date: 6/20/15
 * Time: 4:06 AM
 */
require "twitteroauth-master/autoload.php";
require "config.php";
use Abraham\TwitterOAuth\TwitterOAuth;

session_start();
$request_token = [];
$request_token['oauth_token'] = $_SESSION['oauth_token'];
$request_token['oauth_token_secret'] = $_SESSION['oauth_token_secret'];

if (isset($_REQUEST['oauth_token']) && $request_token['oauth_token'] !== $_REQUEST['oauth_token']) {
    header('Location : clearsessions.php');
}

$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $request_token['oauth_token'], $request_token['oauth_token_secret']);
$access_token = $connection->oauth("oauth/access_token", array("oauth_verifier" => $_REQUEST['oauth_verifier']));
$_SESSION['access_token'] = $access_token;
if($connection->getLastHttpCode()==200){
    $_SESSION['status'] = 'verified';
    header( 'Location: ../last_step.php' );
}
?>